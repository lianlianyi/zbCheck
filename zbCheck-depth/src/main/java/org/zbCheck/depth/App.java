package org.zbCheck.depth;

import cn.hutool.core.thread.ThreadUtil;
import entity.commons.UserApiEntity;
import entity.enumtype.ExchangeEnum;
import entity.exchange.DeptResult;
import lombok.extern.slf4j.Slf4j;
import rest.zb.rest.api.RestApiGet;
import rest.zb.rest.api.RestApiPost;

/**
 * 阿超要的盘口定时交易
 * 
 * @author Administrator
 *
 */
@Slf4j
public class App {

	public static void main(String[] args) {

		String ACCESS_KEY = "08b38d11-445f-447f-9372-d255e10b91bd";
		String SECRET_KEY = "ad030fda-86c1-4d9e-b333-fefbc1fb89e8";
		String url_post = "http://tttrade.zb.com/api/";// 测试
//		String url_get = "http://api.zb.com/data/v1";
		String url_get = "http://ttapi.zb.com/data/v1";
		UserApiEntity userApiEntity = new UserApiEntity(ExchangeEnum.zb, "测试环境", ACCESS_KEY, SECRET_KEY);
		String symbol = "zb_qc";
		RestApiGet apiGet = new RestApiGet(url_get, symbol);
		RestApiPost apiPost = new RestApiPost(url_post, symbol, userApiEntity);

		while (true) {
			try {
				DeptResult dept = apiGet.getDept();
				ThreadUtil.execute(new GuaRun(dept, apiPost, 5000));
				ThreadUtil.sleep(1000);
				ThreadUtil.execute(new EatRun(dept, apiPost, 5000));
			} catch (Exception e) {
				log.error("异常", e);
			}
			ThreadUtil.sleep(2000);
		}
	}
}
