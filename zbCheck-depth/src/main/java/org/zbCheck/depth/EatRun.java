package org.zbCheck.depth;

import org.apache.commons.lang.math.RandomUtils;

import cn.hutool.core.convert.Convert;
import entity.exchange.DeptResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import rest.zb.rest.api.RestApiPost;
import rest.zb.rest.entity.TradeResult;

/**
 * 阿超要的盘口定时交易
 * 
 * @author Administrator
 *
 */
@Slf4j
@AllArgsConstructor
public class EatRun implements Runnable {
	private DeptResult dept;
	private RestApiPost apiPost;
	private int amount;

	public void run() {
		double price = 0;
		if(RandomUtils.nextBoolean()) {
			price = dept.getAsks().get(0).getPrice();
			TradeResult buy = apiPost.buy(price, Convert.toStr(RandomUtils.nextInt(amount)));
			log.info("买入吃单,价:"+price+buy);
		}else {
			price = dept.getBids().get(0).getPrice();
			TradeResult sell = apiPost.sell(price, RandomUtils.nextInt(amount));
			log.info("卖出吃单,价:"+price+sell);
		}
		
		
	}
}
