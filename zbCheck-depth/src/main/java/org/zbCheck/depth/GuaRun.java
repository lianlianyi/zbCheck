package org.zbCheck.depth;

import org.apache.commons.lang.math.RandomUtils;

import cn.hutool.core.convert.Convert;
import entity.exchange.DeptResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import rest.zb.rest.api.RestApiPost;
import rest.zb.rest.entity.TradeResult;

@Slf4j
@AllArgsConstructor
public class GuaRun implements Runnable {
	private DeptResult dept;
	private RestApiPost apiPost;
	private int amount;

	public void run() {
		this.buDepth();
		
		double price = 0;
		if (RandomUtils.nextBoolean()) {
			price = dept.getBids().get(RandomUtils.nextInt(3)).getPrice();
			TradeResult buy = apiPost.buy(price, Convert.toStr(RandomUtils.nextInt(amount)));
			log.info("买入挂单,价:" + price + buy);
		} else {
			price = dept.getAsks().get(RandomUtils.nextInt(3)).getPrice();
			TradeResult sell = apiPost.sell(price, RandomUtils.nextInt(amount));
			log.info("卖出挂单,价:" + price + sell);
		}
	}

	/**
	 * 深度不足,补充深度
	 */
	private void buDepth() {
		int d = 5;
		if (dept.getAsks().size() < d) {
			double price = dept.getAsks().get(0).getPrice();
			for (int i = 0; i < d; i++) {
				price += price * 0.1;
				TradeResult sell = apiPost.sell(price, RandomUtils.nextInt(amount));
				log.info("卖出挂单,价:" + price + sell);
			}
		}
		if (dept.getBids().size() < d) {
			double price = dept.getBids().get(0).getPrice();
			for (int i = 0; i < d; i++) {
				price -= price * 0.1;
				TradeResult sell = apiPost.buy(price, Convert.toStr(RandomUtils.nextInt(amount)));
				log.info("卖出挂单,价:" + price + sell);
			}
		}

	}
}
