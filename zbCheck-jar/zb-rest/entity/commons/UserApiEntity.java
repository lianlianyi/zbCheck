package entity.commons;

import java.io.Serializable;

import entity.enumtype.ExchangeEnum;
import jodd.db.oom.meta.DbTable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@Accessors(chain = true)
@DbTable(value = "UserApiEntity")
/** 用户key */
public class UserApiEntity implements Serializable{
	private static final long serialVersionUID = 2532495564964592602L;
	private String id;
	private String exchange;
	/**自定义用户名*/
	private  String userName;
	/**apiKey*/
	private String apiKey;
	/**secretKey*/
	private String secretKey;
	/**兼容chbtc,可以不写*/
	private String payPass;
	/** Unix 时间戳-创建 */
	private long createTime;
	
	
	public UserApiEntity(ExchangeEnum exchange,String userName, String apiKey, String secretKey) {
		super();
		this.userName = userName;
		this.apiKey = apiKey;
		this.secretKey = secretKey;
		this.exchange = exchange.name();
		this.createTime = System.currentTimeMillis();
	}


	public ExchangeEnum getExchangeEnumType() {
		return ExchangeEnum.getEnum(exchange);
	}


	public void setExchangeEnum(ExchangeEnum exchange) {
		this.exchange = exchange.name();
	}
	
	
}
