DROP TABLE IF EXISTS `UserApiEntity implements Serializable`;
CREATE TABLE `UserApiEntity implements Serializable` (
`id` 
BIGINT
COMMENT '主键'
NOT NULL AUTO_INCREMENT
,
`exchange` 
varchar(800)
NULL 
,
`userName` 
varchar(800)
COMMENT '自定义用户名'
NULL 
,
`apiKey` 
varchar(800)
COMMENT 'apiKey'
NULL 
,
`secretKey` 
varchar(800)
COMMENT 'secretKey'
NULL 
,
`payPass` 
varchar(800)
COMMENT '兼容chbtc,可以不写'
NULL 
,
`createTime` 
datetime
COMMENT 'Unix 时间戳-创建'
NULL 
,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户key';
