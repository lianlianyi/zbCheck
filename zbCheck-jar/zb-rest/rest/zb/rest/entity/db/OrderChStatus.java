package rest.zb.rest.entity.db;

import lombok.Getter;
import lombok.Setter;

/**
 * 挂单状态
 */
public enum OrderChStatus {
	/**0.待成交*/
	dealNot(0, "待成交 "),
	/**1.取消*/
	cancel(1, "取消 "),
	/**2.交易完成*/
	dealOk(2, "交易完成"), 
	/**3.待成交未交易部分*/
	dealNotOver(3, "待成交未交易部分");

	/** 挂单状态（0、待成交 1、取消 2、交易完成 3、待成交未交易部份） */
	private @Getter @Setter int status;
	private @Getter @Setter String name;

	private OrderChStatus(int status, String name) {
		this.status = status;
		this.name = name;
	}

	/** 通过value获取对应的枚举对象*/
	public static OrderChStatus getEnum(int value) {
		for (OrderChStatus examType : OrderChStatus.values()) {
			if (examType.getStatus() == value) {
				return examType;
			}
		}
		return null;
	}
}
