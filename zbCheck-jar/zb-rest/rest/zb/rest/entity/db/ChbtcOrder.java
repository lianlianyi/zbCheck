package rest.zb.rest.entity.db;

import java.sql.Timestamp;
import java.util.Date;

import entity.enumtype.SymbolEnum;
import jodd.db.oom.meta.DbColumn;
import jodd.db.oom.meta.DbId;
import jodd.db.oom.meta.DbTable;
import jodd.vtor.constraint.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;


@Accessors(chain= true)
@Data
@DbTable(value = "ChbtcOrder")
/**chbtc-订单表*/
public class ChbtcOrder {
	/**委托的挂单号*/
	private @NotNull @DbId(value = "id") long id;
	/**挂单类型 1/0[buy/sell]*/
	private @DbColumn int type;
	/**单价*/
	private @DbColumn float price;
	/** 交易手续费，卖单的话，显示的是收入货币（如人民币）；买单的话，显示的是买入货币（如etc） */
	private @DbColumn float fees;
	/**挂单状态（0、待成交 1、取消 2、交易完成 3、待成交未交易部份）*/
	private @DbColumn int status;
	/**挂单总数量*/
	private @DbColumn float total_amount;
	/**已成交数量*/
	private @DbColumn float trade_amount;
	/**Unix 时间戳*/
	private @DbColumn Timestamp trade_date;
	/**已成交总金额*/
	private @DbColumn float trade_money;
	/** 交易类型（目前仅支持btc_cny/ltc_cny/eth_cny/eth_btc/etc_cny） */
	private @DbColumn String currency;
	/**订单重新下单的id(只有取消的状态才有)*/
	private @DbColumn long id_next_retrade;
	/**备注*/
	private @DbColumn(value="remark") String remark;
	
	public OrderChStatus getStatusEnum() {
		return OrderChStatus.getEnum(status);
	}
	public void setTradeDate(Date trade_date) {
		this.trade_date = new Timestamp(trade_date.getTime());
	}
	
	public SymbolEnum getCurrency() {
		return SymbolEnum.getEnum(currency);
	}
	public void setCurrency(SymbolEnum symbol) {
		this.currency = symbol.toString();
	}
}