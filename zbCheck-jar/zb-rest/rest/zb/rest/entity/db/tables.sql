DROP TABLE IF EXISTS `ChbtcNet`;
CREATE TABLE `ChbtcNet` (
`id` 
BIGINT
COMMENT '主键'
NOT NULL AUTO_INCREMENT
,
`createTime` 
datetime
COMMENT 'Unix 时间戳'
NULL 
,
`net` 
float
COMMENT '统计净资产'
NULL 
,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='chbtc-订单表';
DROP TABLE IF EXISTS `ChbtcOrder`;
CREATE TABLE `ChbtcOrder` (
`id` 
BIGINT
COMMENT '委托的挂单号'
NOT NULL AUTO_INCREMENT
,
`type` 
int(11)
NULL 
,
`price` 
float
COMMENT '单价'
NULL 
,
`fees` 
float
COMMENT '交易手续费，卖单的话，显示的是收入货币（如人民币）；买单的话，显示的是买入货币（如etc）'
NULL 
,
`status` 
int(11)
COMMENT '挂单状态（0、待成交 1、取消 2、交易完成 3、待成交未交易部份）'
NULL 
,
`total_amount` 
float
COMMENT '挂单总数量'
NULL 
,
`trade_amount` 
float
COMMENT '已成交数量'
NULL 
,
`trade_date` 
datetime
COMMENT 'Unix 时间戳'
NULL 
,
`trade_money` 
float
COMMENT '已成交总金额'
NULL 
,
`currency` 
varchar(800)
NULL 
,
`id_next_retrade` 
BIGINT
COMMENT '订单重新下单的id(只有取消的状态才有)'
NULL 
,
`remark` 
varchar(800)
COMMENT '备注'
NULL 
,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='chbtc-订单表';
