package rest.zb.config;

import com.jfinal.kit.Prop;

import entity.commons.UserApiEntity;
import entity.enumtype.ExchangeEnum;
import entity.enumtype.KTimeEnum;
import entity.enumtype.SymbolEnum;
import jodd.typeconverter.Converter;
import lombok.extern.slf4j.Slf4j;
import rest.zb.rest.api.RestApiGet;
import rest.zb.rest.api.RestApiPost;

@Slf4j
public class ConfigChbtc {
	private static final Converter Convert = new Converter();
	/**默认使用的币*/
	public static SymbolEnum symbolType;
	/**默认使用的周期*/
	public static KTimeEnum ktimeType;
	
	public static RestApiGet apiGet;
	public static RestApiPost apiPost;
	public static UserApiEntity userApi;
	
	/**符合条件每次交易的量*/
	public static float tradeSize;
	/**每次追加的量*/
	public static float tradeSizeAdd;
	/**每次减仓的量*/
	public static float tradeSizeSubtract;
	static{
		Prop okProp = new Prop("config_chbtc.txt");
		initConfigOkcoin(okProp);//初始化配置
		
//		QuartzPlugin qz = new QuartzPlugin("job_chbtc.txt");
//		qz.version(QuartzPlugin.VERSION_1);
//		qz.start();
//		log.info("开始统计净资产:chbtc");
	}
	
	private static void initConfigOkcoin(Prop prop) {
		ktimeType = KTimeEnum.min1;
		
		tradeSize = Convert.toFloatValue(prop.get("tradeSize"));
		tradeSizeAdd = Convert.toFloatValue(prop.get("tradeSizeAdd"));
		tradeSizeSubtract = Convert.toFloatValue(prop.get("tradeSizeSubtract"));
		
		
		apiGet = new RestApiGet(symbolType);
		userApi = new UserApiEntity(ExchangeEnum.zb,prop.get("userName"),prop.get("AccessKey").trim(), prop.get("Secretkey").trim());
//		userApi = new UserApiEntity("lianlianyi@vip.qq.com_chbtc",prop.get("AccessKey").trim(), prop.get("Secretkey").trim());
		apiPost = new RestApiPost(symbolType,userApi);
		log.info("初始化chbtc配置成功");
	}
}
