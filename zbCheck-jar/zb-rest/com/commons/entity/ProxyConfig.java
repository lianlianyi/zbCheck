package com.commons.entity;

import java.io.File;
import java.io.IOException;

import com.jfinal.kit.PathKit;

import jodd.props.Props;
import lombok.AllArgsConstructor;
import lombok.Data;

public class ProxyConfig {

	public static Proxy proxy;
	
	public static void main(String[] args) {
		System.out.println(ProxyConfig.proxy);
	}

	static {
		String path = PathKit.getPath(ProxyConfig.class);
		path = path.substring(0, path.indexOf("classes") + 7);
		Props prop = new Props();
		try {
			prop.load(new File(path + "/proxy.pros"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		proxy = new Proxy(prop.getBooleanValue("proxy.mode"), prop.getValue("proxy.ip"), prop.getIntegerValue("proxy.port"), prop.getValue("proxy.userName"), prop.getValue("proxy.userPassword"));
	}

	@AllArgsConstructor
	@Data
	public static class Proxy {
		/** 开关,如果正式环境下不需要代理则false */
		private boolean mode;
		private String ip;
		private int port;
		private String userName;
		private String userPassword;
	}
}
