package zbCheck.cli.log;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.jfinal.kit.PathKit;

import jodd.io.FileUtil;

public class Log {
	
	private final String path = PathKit.getRootClassPath() + "/" + new SimpleDateFormat("MM月dd日").format(new Date());

	public void info(String log) {
		String dateStr = this.createFolder();
		try {
			String str = dateStr + log;
			System.out.println(str);
			FileUtil.appendString(new File(path + "/right.log"), str + "\r\n");
		} catch (IOException e) {
			System.out.println("写入right失败");
		}
	}

	public void error(String log)  {
		String dateStr = this.createFolder();
		try {
			String str = dateStr + log;
			System.out.println(str);
			FileUtil.appendString(new File(path + "/error.log"), str + "\r\n");
		} catch (IOException e) {
			System.out.println("写入error失败");
		}
	}

	private String createFolder() {
		String dateStr = new SimpleDateFormat("MM月dd日HH时mm分ss秒").format(new Date()) + ": ";
		if (!FileUtil.isExistingFile(new File(path))) {
			try {
				FileUtil.mkdir(new File(path));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return dateStr;
	}
}
