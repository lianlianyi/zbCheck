package zbCheck.cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import cn.hutool.core.thread.ThreadUtil;
import lombok.extern.slf4j.Slf4j;
import zbCheck.cli.log.Log;
import zbCheck.core.ZbTest;

@Slf4j
public class Cli {
	private String[] args;
	private Options options = new Options();

	public Cli(String[] args) {
		this.newRun();
		this.args = args;

		Option helpCmd = Option.builder("h").desc("show help").build();
		options.addOption(helpCmd);

		Option address = Option.builder("address").hasArg(true).desc("Source wallet address").build();
		Option sendFrom = Option.builder("from").hasArg(true).desc("Source wallet address").build();
		Option sendTo = Option.builder("to").hasArg(true).desc("Destination wallet address").build();
		Option sendAmount = Option.builder("amount").hasArg(true).desc("Amount to send").build();

		options.addOption(address);
		options.addOption(sendFrom);
		options.addOption(sendTo);
		options.addOption(sendAmount);
	}
	public static boolean flag = false;
	private void newRun() {
		Log log = new Log();
		log.info("");
		log.info("");
		log.info("");
		log.info("");
		log.info("");
		log.info("");
		log.info(
				"==================================================新的执行==================================================");
		if(flag) {
			ThreadUtil.sleep(1000000000000l);	
		}
		flag = true;
	}

	/**
	 * 命令行解析入口
	 */
	public void parse() {
		this.validateArgs(args);
		try {
			ZbTest zbTest = new ZbTest();
			CommandLineParser parser = new DefaultParser();
			CommandLine cmd = parser.parse(options, args);
			if (args[0].equals("qc") || args[0].equals("usdt") || args[0].equals("btc") || args[0].equals("zb")) {
				zbTest.trading(args[0]);
			} else {
				System.out.println("交易区设置错误,只能是qc,usdt,btc");
			}

		} catch (Exception e) {
			log.error("Fail to parse cli command ! ", e);
		} finally {

		}
	}

	/**
	 * 验证入参
	 *
	 * @param args
	 */
	private void validateArgs(String[] args) {
		if (args == null || args.length < 1) {

		}
	}
}
