package zbCheck.core;

import java.io.File;
import java.util.List;
import java.util.Set;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.Prop;

import cn.hutool.core.thread.ThreadUtil;
import entity.commons.UserApiEntity;
import entity.enumtype.ExchangeEnum;
import entity.exchange.DeptResult.Dept;
import rest.zb.rest.api.RestApiGet;
import rest.zb.rest.api.RestApiPost;
import rest.zb.rest.entity.Order;
import rest.zb.rest.entity.TradeResult;
import rest.zb.rest.entity.post.Account.Result.Coin;
import zbCheck.cli.log.Log;

public class ZbTest {
	private static UserApiEntity user = null;

	public void init() {
		Prop p = new Prop(new File(PathKit.getRootClassPath() + "/config.pros"));
		user = new UserApiEntity(ExchangeEnum.zb, "test", p.get("user.accessKey"), p.get("user.secretKey"));
	}

	private Log log = new Log();

	public void trading(String tradingArea) {
		this.init();
		if (!this.check()) {
			return;
		}
		RestApiGet get = new RestApiGet("bcc_qc");
		String markets = get.getMarkets();
		JSONObject marketsObj = JSONObject.parseObject(markets);
		Set<String> keySet = marketsObj.keySet();
		RestApiPost post = null;
		for (String symbol : keySet) {
			try {
				// 交易区匹配,btc,qc,usdt
				if (!tradingArea.equals(symbol.split("_")[1])) {
					continue;
				}
				JSONObject market = marketsObj.getJSONObject(symbol);
				String amount = this.genDouble(market.getIntValue("amountScale"));
				post = new RestApiPost(symbol, user);
				Coin coinBefore = post.getAccount().getResult().getCoin(symbol.split("_")[0]);
				log.info("下单前资金,可用" + coinBefore.getAvailable() + ",冻结:" + coinBefore.getFreez());

				get = new RestApiGet(symbol);// 重新配置symbol
				List<Dept> dept = get.getDept().getBids();
				log.info("深度:"+dept);
				double priceBuy = dept.get(dept.size() - 1).getPrice();// 拿出卖5的价进行挂单

				TradeResult buy = post.buy(priceBuy, amount);
				String logStr = symbol + "下单最小量:" + amount + ",价格:" + priceBuy + ",深度:" + (dept.size() - 1) + ",下单结果:"
						+ buy;
				if (buy.getCode() == 1000) {
					log.info(logStr);
					while (true) {
						Thread.sleep(1000);
						Order order = post.getOrder(buy.getId());
						log.info("订单状态:" + order);
						// status : 挂单状态(1：取消,2：交易完成,0/3：待成交/待成交未交易部份)
						if (order.getStatus() == 1 || order.getStatus() == 2) {
							log.info("订单已经取消或者交易成功,ok!!!!!!");
							break;
						}
						TradeResult cancelOrder = post.cancelOrder(buy.getId());
						log.info("订单取消:" + cancelOrder);
					}
				} else {
					log.error(logStr);
				}
				// 资金的比较
				Coin coinAfter = post.getAccount().getResult().getCoin(symbol.split("_")[0]);
				log.info("取消后资金,可用" + coinAfter.getAvailable() + ",冻结:" + coinAfter.getFreez());
				if (coinBefore.getAvailable() != coinAfter.getAvailable()
						|| coinBefore.getFreez() != coinAfter.getFreez()) {
					log.info(symbol + "资金不匹配,前可用:" + coinBefore.getAvailable() + ",后可用:" + coinAfter.getAvailable()
							+ ",前冻结:" + coinBefore.getFreez() + ",后冻结:" + coinAfter.getFreez());
					log.error(symbol + "资金不匹配,前可用:" + coinBefore.getAvailable() + ",后可用:" + coinAfter.getAvailable()
							+ ",前冻结:" + coinBefore.getFreez() + ",后冻结:" + coinAfter.getFreez());
				} else {
					log.info(symbol + "资金匹配,前可用:" + coinBefore.getAvailable() + ",后可用:" + coinAfter.getAvailable()
							+ ",前冻结:" + coinBefore.getFreez() + ",后冻结:" + coinAfter.getFreez());
				}

				log.info("-----------------------------------------------------------");
			} catch (Exception e) {
				log.error(symbol + "执行失败");
			}
			ThreadUtil.sleep(2000);
		}
	}

	private boolean check() {
		if (user == null) {
			return false;
		}
		return true;
	}

	/**
	 * 生成最小位数
	 * 
	 * @param size
	 * @return
	 */
	private String genDouble(int size) {
		if (size == 0) {
			return "1";
		}
		String money = "0.";
		for (int i = 1; i < size; i++) {
			money += "0";
		}
		money += "1";
		return money;
	}
}
